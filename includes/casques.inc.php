

<?php

require_once '../vendor/autoload.php'; // Autochargement des dépendances

try {
    // Requête SQL
    $cnx = new PDO('mysql:host=127.0.0.1;dbname=nolark', 'root', '');
    $req = 'SELECT casque.id, nom, modele, libelle, prix, classement, image, stock FROM casque';
    $req .= ' INNER JOIN marque ON casque.marque=marque.id';
    $req .= ' INNER JOIN type ON casque.type=type.id';
    $req .= ' WHERE libelle="' . substr($pageActuelle, 0, -4) . '"';
    $res = $cnx->prepare($req);
    $res->execute();
    $res->fetch(PDO::FETCH_OBJ);
   
    if (isset($_POST['id']) && isset($_SESSION['userID'])) {
       
        $requete = 'INSERT INTO panier (id_casque , id_user ) values(' . $_POST['id'] . ',' . $_SESSION['userID'] . ' )';
        $requete = $cnx->prepare($requete);
        $requete->execute();
        
    }


    unset($cnx); // Fermeture connexion
    $loader = new Twig\Loader\FilesystemLoader('../tpl'); // Rép. vers les templates
    // Initialisation de l'environnement Twig
    $twig = new Twig\Environment($loader, array(
        'cache' => false,
    ));
   
   
    
    $template = $twig->load('casques.twig'); // Chargemement du template
    // Affectation des variables du template
    echo $template->render(array(
        'casques' => $res
    ));
} catch (PDOException $e) {
    echo 'Erreur: ' . $e->getMessage();
}


