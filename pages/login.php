<?php
 
session_start(); // DETECTION SI CONNECTE
include '../includes/database.php';// IMPORTE METHODE DANS DATABASE
 
if (isset($_SESSION['userEmail'])) { // VERIFIE SI UTILISATEUR EST CONNECTE OU NON
    header('Location:../index.php');
}
 
if (isset($_POST['submit'])) {
 
    $email = htmlspecialchars($_POST['email']); // VERIFICATION
    $password = sha1($_POST['password']);
 
    if ((!empty($email)) && (!empty($password))) {
 
        $database = getPDO();              // CONNECTION BDD
        $requestUser = $database->prepare("SELECT * FROM users WHERE user_email = ? AND user_password = ?");
        $requestUser->execute(array($email, $password));
        $userCount = $requestUser->rowCount(); // TROUVER MAIL ET MDP DANS BDD
        if ($userCount == 1) {              // SI IL TROUVE
           
            $userInfo = $requestUser->fetch(); // CREATION SESSION UTILISATEUR
            $_SESSION['userID'] = $userInfo['user_id'];
            $_SESSION['userPseudo'] = $userInfo['user_pseudo'];
            $_SESSION['userEmail'] = $userInfo['user_email'];
            $_SESSION['userPassword'] = $userInfo['user_password'];
            $_SESSION['userAdmin'] = $userInfo['isadmin'];
            $_SESSION['userRegisterDate'] = $userInfo['registerdate'];
            $succesMessage = 'Bravo, vous êtes maintenant connecté !';
            header('refresh:3;url=../index.php');
 
        } else {
            $errorMessage = 'Email ou mot de passe incorrect!';
        }
    } else {
        $errorMessage = 'Veuillez remplir tous les champs..';
    }
}
 
?>
 
<html lang="fr-FR">
    <head>
        <title>Espace Client - Connexion</title>
        <meta charset="UTF-8">
        <meta name="author" content="José GIL">
        <meta name="description" content="Découvrez des casques moto dépassant même les exigences des tests de sécurité. Tous les casques Nolark au meilleur prix et avec en prime la livraison gratuite !">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/casque.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <header>
              <?php
            include('../includes/header.html.inc.php');
           ?>
        </header>
        <div class="text-center">
            <h3>Espace Client - Connexion</h3>
            
        </div>
        <div class="form-div text-center">
            <h3>Connexion</h3>
            <?php if (isset($errorMessage)) { ?> <p style="color: red;"><?= $errorMessage ?></p> <?php } ?>
            <?php if (isset($succesMessage)) { ?> <p style="color: green;"><?= $succesMessage ?></p> <?php } ?>
            <form method="post" action="">
 
                <span>Adresse Email :</span><br>
                <input type="email" name="email" placeholder="Email"><br>
 
                <span>Mot de passe :</span><br>
                <input type="password" name="password" placeholder="Mot de passe"><br><br>
 
                <input type="submit" name="submit" value="Se connecter">
            </form>
        </div>
        <footer>
             <?php
            include('../includes/footer.inc.php');
            ?>
        </footer>
    </body>
</html>