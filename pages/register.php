<?php

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

session_start(); // DETECTION SI CONNECTE
include '../includes/database.php'; // IMPORTER LA METHODE DANS DATABASE



if (isset($_POST['submit'])) {

    $pseudo = htmlspecialchars($_POST['pseudo']); // VARIABLE FORMULAIRE DANS LES VARIABLES PHP PAR POST ( CAR GET AFFICHE DANS URL)
    $email = htmlspecialchars($_POST['email']);
    $password = sha1($_POST['password']);
    $password_confirm = sha1($_POST['password_confirm']);
    date_default_timezone_set('Europe/Paris');
    $date = date('d/m/Y à H:i:s');

    if ((!empty($pseudo)) && (!empty($email)) && (!empty($password_confirm)) && (!empty($password))) { // VERIFIE SI EMAIL PAS VIDE
        if (strlen($pseudo) <= 16) {    // SI PSEUDO PAS PLUS GRAND QUE 16                             // ET AFFICHER ERREUR EN FONCTION
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) { // ONT VERIFIE EMAIL VALIDE
                if ($password == $password_confirm) { // VERIFIE MDP IDENTIQUE
                    $database = getPDO(); // ONT RECUP LE GET PDO QUI EST INCLUDE
                    $rowEmail = countDatabaseValue($database, 'user_email', $email); // ONT COMPTE LE NB DE MAIL QUI EXISTE PAR RAPP A EMAIL UTLISATEUR
                    if ($rowEmail == 0) { // ONT VERIFIE
                        $rowPseudo = countDatabaseValue($database, 'user_pseudo', $pseudo); // IDEM PSEUDO
                        if ($rowPseudo == 0) {
                            $insertMember = $database->prepare("INSERT INTO users(user_pseudo, user_email, user_password, isadmin, registerdate) VALUES(?, ?, ?, ?, ?)");
                            $insertMember->execute([// ONT CREER LE COMPTE
                                $pseudo,
                                $email,
                                $password,
                                0,
                                $date
                            ]);


                            require '../includes/PHPMailer.php';
                            require '../includes/SMTP.php';
                            require '../includes/Exception.php';
//Create a new PHPMailer instance
                            $mail = new PHPMailer;
//Tell PHPMailer to use SMTP
                            $mail->isSMTP();

//Set the hostname of the mail server
                            $mail->Host = 'smtp.gmail.com';
// use
// $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
                            $mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
                            $mail->SMTPSecure = 'tls';
//Whether to use SMTP authentication
                            $mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
                            $mail->Username = "testbonapartetest83@gmail.com";
//Password to use for SMTP authentication
                            $mail->Password = "B0n@parte";
//Set who the message is to be sent from
                            $mail->setFrom ("testbonapartetest83@gmail.com");
//Set an alternative reply-to address

                            $mail->addAddress($email);
//Set the subject line
                            $mail->Subject = 'Création du compte Nolark';
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
                            $mail->Body = 'Votre compte ' . $pseudo . ' a été créé sur norlark .';

                           
                         $mail->send();
 




                            $succesMessage = "Votre compte à bien été créé !";
                            header('refresh:3;url=login.php');
                        } else {
                            $errorMessage = 'Cette pseudo est déjà utilisée..';
                        }
                    } else {
                        $errorMessage = 'Cette email est déjà utilisée..';
                    }
                } else {
                    $errorMessage = 'Les mots de passes ne correspondent pas...';
                }
            } else {
                $errorMessage = "Votre email n'est pas valide...";
            }
        } else {
            $errorMessage = 'Le pseudo est trop long...';
        }
    } else {
        $errorMessage = 'Veuillez remplir tous les champs...';
    }
}
?>

<!DOCTYPE html>
<html lang="fr-FR">
    <head>
        <title>Espace Clients inscription</title>
        <meta charset="UTF-8">
        <meta name="author" content="José GIL">
        <meta name="description" content="Découvrez des casques moto dépassant même les exigences des tests de sécurité. Tous les casques Nolark au meilleur prix et avec en prime la livraison gratuite !">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/casque.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <header>
            <?php
            include('../includes/header.html.inc.php');
            ?>
        </header>
        <div class="text-center">
            <h3>Espace Client - Inscription</h3>

        </div>
        <div class="form-div text-center">
            <h3>Inscription</h3>
            <?php if (isset($errorMessage)) { ?> <p style="color: red;"><?= $errorMessage ?></p> <?php } ?>
            <?php if (isset($succesMessage)) { ?> <p style="color: green;"><?= $succesMessage ?></p> <?php } ?>
            <form method="post" action="">

                <span>Pseudo :</span><br>
                <input type="text" name="pseudo" placeholder="Pseudo" <?php if (isset($pseudo)) { ?>value="<?= $pseudo ?>" <?php } ?>><br>

                <span>Adresse Email :</span><br>
                <input type="email" name="email" placeholder="Email" <?php if (isset($email)) { ?>value="<?= $email ?>" <?php } ?>><br>

                <span>Mot de passe :</span><br>
                <input type="password" name="password" placeholder="Mot de passe"><br>

                <span>Confirmation Mot de passe :</span><br>
                <input type="password" name="password_confirm" placeholder="Confirmation Mot de passe"><br><br>

                <input type="submit" name="submit" value="S'inscrire">
            </form> 
        </div>
        <footer>
            <?php
            include('../includes/footer.inc.php');
            ?>
        </footer>
    </body>

</html>