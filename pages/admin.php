<?php 

session_start();
include '../includes/database.php'; 

if (!isset($_SESSION['userEmail'])) {
    header('Location:espace-client');
}

$database = getPDO();
$members = $database->query('SELECT * FROM users');

$clientID;
$userCount;
$userInfo;
if (isset($_GET['id'])) {
    $clientID = $_GET['id'];
    $requestUser = $database->prepare("SELECT * FROM users WHERE user_id = ?");
    $requestUser->execute(array($clientID));
    $userCount = $requestUser->rowCount();
    $userInfo = $requestUser->fetch();
}

// Edit
if (isset($_POST['edit'])) {
    $userEmail = $userInfo['user_email'];
    $userPseudo = $userInfo['user_pseudo'];
    $emailValue = htmlspecialchars($_POST['email']);
    $pseudoValue = htmlspecialchars($_POST['pseudo']);

    if ($userEmail != $emailValue) {
        $rowEmail = countDatabaseValue($database, 'user_email', $emailValue);
        if ($rowEmail == 0) {
            $request = $database->prepare("UPDATE users SET user_email = ? WHERE user_email = ?");
            $request->execute([
                $emailValue,
                $userEmail
            ]);
            $succesMessage = 'Les informations ont bien été modifiés!';
        } else {
            $errorMessage = 'Cette adresse email existe déjà !';
        }
        header('refresh:3;url=admin.php');
    }

    if ($userPseudo != $pseudoValue) {
        $rowPseudo = countDatabaseValue($database, 'user_pseudo', $pseudoValue);
        if ($rowPseudo == 0) {
            $request = $database->prepare("UPDATE users SET user_pseudo = ? WHERE user_pseudo = ?");
            $request->execute([
                $pseudoValue,
                $userPseudo
            ]);
            $succesMessage = 'Les informations ont bien été modifiés!';
        } else {
            $errorMessage = 'Ce pseudo existe déjà !';
        }
        header('refresh:3;url=admin.php');
    }
}

// Suppresion du Compte
if (isset($_POST['delete'])) {
    $request = $database->prepare('DELETE FROM users WHERE user_email = ?');
    $request->execute([
        $userInfo['user_email'],
    ]);
    header('Location:admin.php');
}

// Ban Système
//if ((isset($_POST['ban'])) || isset($_POST['unban'])) {

  //  $banValue = 0;
   // if ($userInfo['isban'] == 0) {
   //     $banValue = 1;
  //  }
  //  $request = $database->prepare("UPDATE users SET isban = ? WHERE user_email = ?");
  //  $request->execute([
   //     $banValue,
   //     $userInfo['user_email']
  //  ]);
  //  $succesMessage = "Vous venez de " . ($banValue == 0 ? 'Débannir' : 'Bannir') . ' le client!';
  //  header('refresh:3;url=admin.php');
//}

?>

<!DOCTYPE html>
<html lang="fr-FR">
    <head>
        <title>Espace Client - Admin</title>
        <meta charset="UTF-8">
        <meta name="author" content="José GIL">
        <meta name="description" content="Découvrez des casques moto dépassant même les exigences des tests de sécurité. Tous les casques Nolark au meilleur prix et avec en prime la livraison gratuite !">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/casque.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <header>
              <?php
            include('../includes/header.html.inc.php');
           ?>
        </header>
        <div class="text-center">
            <h3>Espace Client - Admin</h3>
            
        </div>
        <div class="form-div text-center">
            <h3>Panel Admin</h3>
            <?php if ($_SESSION['userAdmin'] == 1) { ?>
                <ul>
                    <?php while($users = $members->fetch()) {?>
                        <li><?= $users['user_email'] ?> - <?= $users['user_pseudo'] ?> - <a href="admin.php?id=<?= $users['user_id'] ?>">Gérer</a></li>
                    <?php } ?>
                </ul>
                
            
                <?php if (isset($clientID)) { 
                    if ($userInfo == null) { echo "Ce compte n'existe pas !"; } else { ?>
                    <h3>Gérer : <?= $userInfo['user_pseudo'] ?></h3>
                    <?php if (isset($errorMessage)) { ?> <p style="color: red;"><?= $errorMessage ?></p> <?php } ?>
                    <?php if (isset($succesMessage)) { ?> <p style="color: green;"><?= $succesMessage ?></p> <?php } ?>
                    <form method="post" action="">
                        <span>Email :</span><br>
                        <input type="email" name="email" placeholder="Email" value="<?= $userInfo['user_email'] ?>"><br>

                        <span>Pseudo :</span><br>
                        <input type="text" name="pseudo" placeholder="Pseudo" value="<?= $userInfo['user_pseudo'] ?>"><br><br>

                        <input type="submit" name="edit" value="Editer">
                        <input type="submit" name="delete" value="Supprimer le Compte">
                        
                           
                    </form>
                <?php } } ?>

            <?php } else { ?>
                <p style="color: red;">Vous devez être Admin pour accéder à cette page !</p>
            <?php } ?>
        </div>
        <footer>
             <?php
            include('../includes/footer.inc.php');
            ?>
        </footer>
    </body>
</html>


















