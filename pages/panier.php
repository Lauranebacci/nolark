<?php
session_start(); // DETECTION SI CONNECTE
include '../includes/database.php'; // IMPORTER LA METHODE DANS DATABASE
$database = getPDO();
if(isset($_GET['action'])&& $_GET['action']=='supprimer' ){
 $supprimer=   $database->prepare('Delete from panier where identifiant = ?');
    
 $supprimer->execute(array($_GET['id']));
}
$clientID = $_SESSION['userID'];
$casque = $database->prepare("SELECT * FROM panier inner join casque on id_casque = casque.id WHERE id_user  = ?");
$casque->execute(array($clientID));
$data = $casque->fetchAll();

?>
<!DOCTYPE html>
<html lang="fr-FR">
    <head>
        <title>Panier</title>
        <meta charset="UTF-8">
        <meta name="author" content="José GIL">
        <meta name="description" content="Découvrez des casques moto dépassant même les exigences des tests de sécurité. Tous les casques Nolark au meilleur prix et avec en prime la livraison gratuite !">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/casque.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <header>
            <?php
            include('../includes/header.html.inc.php');
            ?>
        </header>
        <table>
            <thead>
                <tr>
                    <th>Modele</th>
                    <th>Prix</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $row) {($row) ?> 
                    <tr>
                        <td> <?= $row['modele'] ?> </td>
                        <td><?= $row['prix'] ?> </td>
                        <td><a href="panier.php?action=supprimer&id=<?= $row['identifiant'] ?>">Supprimer</a></td>

                    </tr>

                <?php } ?>
            </tbody>
        </table> 
        <footer>
            <?php
            include('../includes/footer.inc.php');
            ?>
        </footer>

    </body>
</html>





