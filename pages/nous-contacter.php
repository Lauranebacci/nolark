<!DOCTYPE html>
<!-- 
     Page web créé dans le cadre du cours de web Dev le 01/09/2018
     Auteur : José GIL
     Email : jgil83000@gmail.com
-->
<?php
    session_start();   
    include'../includes/database.php'      
           ?>
<html lang="fr-FR">
    <head>
        
        <title>Casques Nolark : Sécurité et confort, nos priorités !</title>
        <meta charset="UTF-8">
        <meta name="author" content="José GIL">
        <meta name="description" content="Découvrez des casques moto dépassant même les exigences des tests de sécurité. Tous les casques Nolark au meilleur prix et avec en prime la livraison gratuite !">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/casque.css" rel="stylesheet" type="text/css">
        <script src="../JS/nolark.js" type="text/javascript"></script>
    </head>
    <body>
    <header>
           <?php
            include('../includes/header.html.inc.php');
           ?>
        </header>
        <h1>Nous localiser :</h1>
        <section id="carto">
         <img id="c1" src="https://open.mapquestapi.com/staticmap/v5/map?key=2y12UfMHYJQdytm8gGCC9eL1qDMs8tKG
          &locations=Barjols,Var,France|http://gil83.fr/nolark/marqueur_nolark.png
          &center=Châteauroux,Indre,France&zoom=6&size=600,610"
         alt="Carte OpenStreetMap statique avec le pays de Nolark">

            <img class="cartes" src="https://open.mapquestapi.com/staticmap/v5/map?key=2y12UfMHYJQdytm8gGCC9eL1qDMs8tKG
                 &locations=Rond-Point Bir-Hackeim, Toulon, Var, Provence-Alpes-
                 Côte d'Azur, France métropolitaine, 83000, France
                 |http://gil83.fr/nolark/marqueur_nolark.png
                 &zoom=13&size=306,300"
                 alt="Carte OpenStreetMap statique avec la ville de Nolark">
            <img class="cartes" src="https://open.mapquestapi.com/staticmap/v5/map?key=2y12UfMHYJQdytm8gGCC9eL1qDMs8tKG
                 &locations=43.12115, 5.94037|http://gil83.fr/nolark/marqueur_nolark.png
                 &zoom=18&size=306,300"
                 alt="Carte OpenStreetMap statique avec l'emplacement de Nolark">
        </section>
        <section id="contact">
            <h1>Nous contacter</h1>
            <form id="form_contact" name="form_contact" action="http://gil83.fr/nolark/testforms.php" method="POST">
                <fieldset id="coordo">
                    <legend>Pour mieux vous connaître</legend>
                    <div>
                        <label for="i_nom">Votre nom :</label>
                        <input type="text" name="i_nom" id="i_nom" size="35" 
                               pattern="^[a-zA-Z\s\-]+$"  required>
                    </div>                   
                    <div>
                        <label for="i_pnom">Votre prénom :</label>
                        <input type="text" name="i_pnom" id="i_pnom" size="35" 
                               required>
                    </div>
                    <div>
                        <label for="i_email">Votre e-mail :</label>
                        <input type="email" name="i_email" id="i_email" size="35">
                    </div>
                    <div>
                        <label for="i_url">Votre url :</label>
                        <input type="url" name="i_url" id="i_url" size="35">
                    </div>
                    <div>
                        <label for="lst_typeclient">Vous êtes :</label>
                        <select name="lst_typeclient" id="lst_typeclient">
                            <option value="1">Étudiant</option>
                            <option value="2">Salarié du privé</option>
                            <option value="3">Salarié du public</option>
                            <option value="4">Chef d'entreprise</option>
                            <option value="5">Journaliste</option>
                            <option value="6">Professionnel(le) du casque</option>
                            <option value="99">Autre statut</option>
                        </select>
                    </div>
        <div>
           <label for="i_date">Date de naissance :</label>
            <input type="number" name="num_jour" id="num_jour" min="1" max="31">
                        
             <input type="number" name="num_mois" id="num_mois" min="1" max="12">
                        
           <input type="number" name="num_annee" id="num_annee" min="1900" max="2020">
                    </div>
                    <fieldset id="marques">
              <legend>Vos marques préférés</legend>
              <div>
              <input type="checkbox" name="chk_marques[0]" id="chk_agv" value="agv">
               AGV 
             </div>
             <div>
              <input type="checkbox" name="chk_marques[1]" id="chk_airoh" value="airoh">
               Airoh
             </div>
             <div>
                <input type="checkbox" name="chk_marques[2]" id="chk_Araï" value="araï">
                Araï
                </div>
                  <div>
                 <input type="checkbox" name="chk_marques[3]" id="chk_bell" value="bell">
                  Bell
               </div>
                 <div>
                 <input type="checkbox" name="chk_marques[4]" id="chk_bering" value="bering">
                   Bering
                </div>
                   <div>
                   <input type="checkbox" name="chk_marques[5]" id="chk_bultaco" value="bultaco">
                     Bultaco
                  </div>
                   <div>
                    </fieldset>
                    <div>
                  <label for="votre_couleur_pref">Votre couleur préférée :</label>
                   <input type="color" name="votre_couleur_pref" id="votre_couleur_pref">
                    </div>
                </fieldset>
                <fieldset id="motif">
                    <legend>Motif du contact</legend>
                    <div>
                   <input type="radio" name="motif">
                    Demande d'information
                    </div>
                    <div>
                    <input type="radio" name="motif">
                     Réclamation
                    </div>
                    <div>
                   <input type="radio" name="motif">
                    Satisfaction
                    </div>
                    <div>
                    <input type="radio" name="motif">
                     Suivi de commande
                    </div>
                    <div>
                    <input type="radio" name="motif">
                      Publicité
                    </div>
                    <div>
                    <input type="radio" name="motif">
                     Autre...
                    </div>
                    <div id='text'>
                     <label for="textarea">
                      Si "Autre", veuillez préciser :
                     </label>
                    <textarea name="textarea">
                         
                     </textarea>
                  </div>
                </fieldset>.
                <div id="boutons">
                 <input type="button" value="Envoyer votre demande de contact"
                  id="btn_envoyer">
                    <input type="reset" value="Effacer les réponses"
                 id="effacer">
                </div>
            </form>

        </section>       


        <footer>
            <?php
            include('../includes/footer.inc.php');
            ?>
        </footer>
        
    </body>
</html>
