<!DOCTYPE html>
<!-- 
     Page web créé dans le cadre du cours de web Dev le 01/09/2018
     Auteur : José GIL
     Email : jgil83000@gmail.com
-->
  <?php
    session_start();  
    include'../includes/database.php'
    
           ?>


<html lang="fr-FR">
    <head>
        <title>Casques Nolark : Sécurité et confort, nos priorités !</title>
        <meta charset="UTF-8">
        <meta name="author" content="José GIL">
        <meta name="description" content="Découvrez des casques moto dépassant même les exigences des tests de sécurité. Tous les casques Nolark au meilleur prix et avec en prime la livraison gratuite !">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/casque.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <header>
              <?php
            include('../includes/header.html.inc.php');
           ?>
        </header>
        <section id="casques">
            <?php include('../includes/casques.inc.php'); ?>

            </section>
        <footer>
             <?php
            include('../includes/footer.inc.php');
            ?>
        </footer>
    </body>
</html>
